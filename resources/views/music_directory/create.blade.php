@extends('base.index')
@section('page-title', 'Music Directory')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="media">
                            <i class="far fa-folder-open mr-2"></i>
                            <div class="media-body">
                                <h5 class="mt-1">Add Path</h5>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="/music-directories">
                            @csrf

                            <div class="form-group row">
                                <label for="path" class="col-md-4 col-form-label text-md-right">Path</label>

                                <div class="col-md-6">
                                    <input id="path" type="text"
                                           class="form-control @error('path') is-invalid @enderror" name="path"
                                           value="{{ old('path') }}" required autocomplete="path" autofocus>

                                    @error('path')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <a href="/music-settings" class="btn btn-outline-primary" title="Cancel">Cancel</a>
                                    <button type="submit" class="btn btn-outline-primary">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
