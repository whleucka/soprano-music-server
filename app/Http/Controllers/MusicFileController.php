<?php

namespace App\Http\Controllers;

use App\MusicFile;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class MusicFileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show file details
     * @param MusicFile $file
     * @return Factory|View
     */
    public function show(MusicFile $file) {
        return view('music_file.show', compact('file'));
    }
}
