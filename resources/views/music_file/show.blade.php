@extends('base.index')
@section('page-title', 'Music File')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="media">
                            <img class="album-cover-art-big mr-1" src="{{ $file->cover }}" alt="cover" title="{{ $file->album }}">
                            <div class="media-body w-100">
                                <h5 class="mt-1 pl-2">
                                    <div class="truncate" title="{{ $file->artist }} - {{ $file->title }}">{{ $file->artist }} - {{ $file->title }}</div>
                                    <div class="truncate" title="{{ $file->album }}">{{ $file->album }}</div>
                                </h5>
                            </div>
                        </div>
                        <div class="pt-3">
                            <audio style="width: 100%;" src="/stream/{{ $file->id }}" controls></audio>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <caption class="caption-top">Music File:</caption>
                                <tbody class="border-bottom">
                                <tr>
                                    <td class="text-right pr-2"><strong>Artist</strong></td>
                                    <td>{{ $file->artist}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Album</strong></td>
                                    <td>{{ $file->album}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Title</strong></td>
                                    <td>{{ $file->title}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2" style="width: 100px;"><strong>ID</strong></td>
                                    <td>{{ $file->id }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>MD5 Path</strong></td>
                                    <td>{{ $file->md5_file_path}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>File Path</strong></td>
                                    <td>{{ $file->file_path}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>File Name</strong></td>
                                    <td>{{ $file->file_name}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>File Size</strong></td>
                                    <td>{{ $file->file_size}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Mime Type</strong></td>
                                    <td>{{ $file->mime_type }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>File Format</strong></td>
                                    <td>{{ $file->file_format}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Bitrate</strong></td>
                                    <td>{{ $file->bitrate}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Cover</strong></td>
                                    <td>{{ $file->cover}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Track No.</strong></td>
                                    <td>{{ $file->track_number}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Playtime</strong></td>
                                    <td>{{ $file->playtime_string}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Playtime Seconds</strong></td>
                                    <td>{{ $file->playtime_seconds}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Year</strong></td>
                                    <td>{{ $file->year}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Created</strong></td>
                                    <td>{{ $file->created_at }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Updated</strong></td>
                                    <td>{{ $file->updated_at }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-primary mt-3" title="Back">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
