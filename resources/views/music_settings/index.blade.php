@extends('base.index')
@section('page-title', 'Music Settings')
@section('content-title', 'Music Settings')

@section('content')
    <section id="music-directories">
        <div class="section-header align-text-bottom">
            <h4>Directories</h4>
        </div>
        <div id="music-settings-table-container" class="table-responsive">
            <table id="music-settings-table" class="table table-sm table-borderless">
                <caption class="caption-top">
                    Directories that will be scanned for audio files. Accepted formats are {{ $accepted_formats }}
                </caption>
                <tbody>
                @forelse ($directories as $dir)
                    <tr>
                        <td class="no-wrap">
                            <div class="truncate">
                                {{ $dir->path }}
                            </div>
                        </td>
                        <td nowrap>
                            <form class="form-inline float-right" action="/music-directories/{{ $dir->id }}"
                                  method="post">
                                @method('DELETE')
                                <button class="btn btn-sm m-1 btn-outline-danger"
                                        title="Delete"
                                        onClick='formConfirm(event, this.form, "Are you sure you want to delete this directory path:\n{{ $dir->path }}", true);'>
                                    <i class="fas fa-trash"></i>
                                </button>
                                @csrf
                            </form>
                            <form class="form-inline float-right" action="/music-settings/scan/{{ $dir->id }}"
                                  method="post">
                                @method('POST')
                                <button class="btn btn-sm m-1 btn-outline-primary"
                                        title="Scan"
                                        onClick='formConfirm(event, this.form, "Are you sure you want to scan this directory path:\n{{ $dir->path }}\nThis action could take a while to complete", true);'>
                                    <i class="fas fa-sync-alt mr-2"></i> Scan
                                </button>
                                @csrf
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">No directories</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="table-actions">
            <div class="no-wrap">
                <a href="/music-directories/create" title="Add new directory path"
                   class="m-1 btn btn-sm btn-outline-primary float-right"><i class="far fa-folder-open mr-2"></i> Add Path</a>
                <form method="POST" class="form-inline float-right" action="/music-settings/scan-all">
                    @csrf
                    <button class="m-1 btn btn-sm btn-outline-primary"
                            onClick="formConfirm(event, this.form, 'Are you sure you want to scan all directories?\nThis action could take a while to complete.', true);"
                            type="submit">
                        <i class="fas fa-sync-alt mr-2"></i> Scan All
                    </button>
                </form>
            </div>
        </div>
    </section>
    {{--<section id="transcoding" class="mt-4">
       <div class="section-header align-text-bottom">
           <h4>Transcoding</h4>

       </div>
   </section>--}}
    {{--<section id="options" class="mt-4">
        <div class="section-header align-text-bottom">
            <h4>Server Options</h4>
        </div>
    </section>--}}
    {{--<section id="cover-art" class="mt-4">
        <div class="section-header align-text-bottom">
            <h4>Cover Art</h4>
        </div>
    </section>--}}
    {{--<section id="advanced-settings" class="mt-4">
        <div class="section-header align-text-bottom">
            <h4>Advanced Settings</h4>
        </div>
    </section>--}}
@endsection
