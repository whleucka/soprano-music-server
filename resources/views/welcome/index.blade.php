@extends('base.index')
@section('page-title', 'Home')

@section('content')
    <div class="jumbotron">
        <div id="jumbo-content" class="mx-auto">
            <h1 class="main-title display-1"><i class="fas fa-music mr-2"></i> {{ config('app.name') }}</h1>
            <p class="lead p-2 mb-2">Music streaming, simplified.</p>
            <div class="supported-devices mt-2">
                <span><i class="fab fa-android"></i> Android</span> | <span><i class="fab fa-apple"></i> Apple</span> | <span><i class="fab fa-windows"></i> Microsoft</span> | <span><i class="fab fa-linux"></i> Linux</span>
            </div>
        </div>
    </div>
    <div class="alert alert-warning" role="alert">
        <strong>Note: </strong> User registration is <em>temporarily</em> disabled
    </div>
@endsection

