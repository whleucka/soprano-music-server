<?php

namespace App\Http\Controllers;

use App\MusicFile;
use App\User;
use FFMpeg;

class MusicStreamController extends Controller
{
    /**
     * Serve a music file
     * @param MusicFile $file
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function stream(MusicFile $file) {
        $path = ($file->file_format !== 'mp3') ?
            $this->transcode($file->file_path) :
            $file->file_path;

        $headers = [
            'Content-Type' => mime_content_type($path),
            'Content-Disposition' => 'inline',
            'Cache-Control' => 'public, max-age=2629746',
            'Accept-Ranges' => 'bytes',
            'Content-Length' => filesize($path),
            'Content-Transfer-Encoding' => 'chunked',
            'Connection' => "Keep-Alive",
            'X-Pad' => 'avoid browser bug',
        ];

        return response()->file($path, $headers);
    }

    private function transcode($file_path) {
        $file_info = pathinfo($file_path);
        $storage_dir = storage_path() . '/app/public/transcode/';
        if (!file_exists($storage_dir))
            mkdir($storage_dir);
        $md5_file = $storage_dir . md5($file_path) . '.mp3';
        if (!file_exists($md5_file)) {
            $ffmpeg = FFMpeg\FFMpeg::create(array(
                'ffmpeg.binaries' => '/usr/bin/ffmpeg',
                'ffprobe.binaries' => '/usr/bin/ffprobe',
                'timeout' => 60*5,
                'ffmpeg.threads' => 12,
            ));
            $audio_channels = 2;
            $bitrate = 320;
            $audio = $ffmpeg->open($file_path);
            $format = new FFMpeg\Format\Audio\Mp3('libmp3lame');
            $format
                ->setAudioChannels($audio_channels)
                ->setAudioKiloBitrate($bitrate);
            try {
                $audio->save($format, $md5_file);
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $md5_file;
    }
}
