<?php

namespace App\Http\Controllers;

use App\UserLikes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MusicLibraryAPIController extends Controller
{
    public function auth() {
        return request()->user();
    }

    public function like() {
        $music_id = request()->music_file;
        $user_id = request()->uuid;
        if ($music_id && $user_id) {
            $user_like = UserLikes::query()->where(['user_id' => $user_id, 'music_files_id' => $music_id])->first();
            if ($user_like) {
                try {
                    $user_like->delete();
                    return ['status' => 'off'];
                } catch (\Exception $e) {
                    error_log("Couldn't remove user like {$e->getMessage()}");
                }
            } else {
                $user_like = new UserLikes;
                $user_like->user_id = $user_id;
                $user_like->music_files_id = $music_id;
                $user_like->save();
                return ['status' => 'on'];
            }
        }
    }

    /**
     * Returns entire music file collection
     * @return Collection
     */
    public function tracks() {
        $searchTerm = isset(request()->search) ? trim(request()->search) : '';
        $user_id = request()->uuid;
        //TODO: currently we don't filter liked music files by user
        if ($searchTerm !== '')
            $files = DB::table('music_files')
                ->select(
                    'id',
                    'album',
                    'artist',
                    'title',
                    'cover',
                    'playtime_string',
                    'playtime_seconds',
                    'year',
                    DB::raw("(SELECT 1 FROM user_likes WHERE music_files_id = music_files.id) as liked"))
                ->orderBy('artist')
                ->orderBy('album')
                ->where('artist', 'like', "{$searchTerm}%")
                ->orWhere('album', 'like', "{$searchTerm}%")
                ->orWhere('title', 'like', "%{$searchTerm}%")
                ->get();
        else
            $files = DB::table('music_files')
                ->select(
                    'id',
                    'album',
                    'artist',
                    'title',
                    'cover',
                    'playtime_string',
                    'playtime_seconds',
                    'year',
                    DB::raw("(SELECT 1 FROM user_likes WHERE music_files_id = music_files.id) as liked"))
                ->orderBy('artist')
                ->orderBy('album')
                ->get();
        return $files;
    }

    public function getLiked() {
        $files = DB::table('music_files')
            ->select(
                'id',
                'album',
                'artist',
                'title',
                'cover',
                'playtime_string',
                'playtime_seconds',
                'year',
                DB::raw("(SELECT 1 FROM user_likes WHERE music_files_id = music_files.id) as liked"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('user_likes')
                    ->whereRaw('user_likes.music_files_id = music_files.id');
            })
            ->orderBy('artist')
            ->orderBy('album')
            ->get();
        return $files;
    }

    /**
     * Returns a distinct artist collection
     * @return Collection
     */
    public function artists() {
        $files = DB::table('music_files')
            ->select('artist')
            ->distinct()
            ->orderBy('artist')
            ->where('artist', '!=', 'No Artist')
            ->orWhere('artist', '!=', "''")
            ->orderBy('artist')
            ->get();
        return $files;
    }

    /**
     * Return artist collection
     * @return Collection|null
     */
    public function artist() {
        $artist = isset(request()->search) ? trim(request()->search) : '';
        if ($artist !== '')
           return DB::table('music_files')
                ->select(
                    'id',
                    'album',
                    'artist',
                    'title',
                    'cover',
                    'playtime_string',
                    'playtime_seconds',
                    'year')
                ->orderBy('artist')
                ->orderBy('album')
                ->where('artist', '=', "{$artist}")
                ->get();
        return null;
    }

    /**
     * Returns distinct album collection
     * @return Collection
     */
    public function albums() {
        $files = DB::table('music_files')
            ->select('album', 'cover', 'year')
            ->where('album', '!=', 'No Album')
            ->orWhere('album', '!=', "''")
            ->groupBy('album')
            ->orderBy('album')
            ->get();
        return $files;
    }

    /**
     * Return album collection
     * @return Collection|null
     */
    public function album() {
        $album = isset(request()->search) ? trim(request()->search) : '';
        if ($album !== '')
            return DB::table('music_files')
                ->select(
                    'id',
                    'album',
                    'artist',
                    'title',
                    'cover',
                    'playtime_string',
                    'playtime_seconds',
                    'year')
                ->orderBy('artist')
                ->orderBy('album')
                ->where('album', '=', "{$album}")
                ->get();
        return null;
    }
}
