@extends('base.index')
@section('page-title', 'Users')
@section('content-title', 'Users')

@section('content')
    <div id="user-table-container" class="table-responsive">
        <table id="user-table" class="table table-sm table-hover mt-3">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse ($users as $user)
                <tr>
                    <td><div class="truncate" title="{{ $user->id }}"><a href="/users/{{ $user->id }}">{{ $user->id }}</a></div></td>
                    <td><div class="truncate" title="{{ $user->name }}">{{ $user->name }}</div></td>
                    <td><div class="truncate" title="{{ $user->email }}">{{ $user->email }}</div></td>
                    <td><div class="truncate" title="{{ $user->role }}">{{ $user->role }}</div></td>
                    <td>
                        @if ( $user->id !== $me->id )
                            <form class="form-inline float-left" action="/users/{{ $user->id }}" method="post">
                                @method('DELETE')
                                <button class="btn btn-sm m-1 btn-outline-danger"
                                        title="Delete"
                                        onClick='formConfirm(event, this.form, "You are about to delete user:\n{{ $user->id }}\n\nThis action cannot be undone\nAre you sure you want to proceed?", true);'>
                                    <i class="fas fa-trash"></i>
                                </button>
                                @csrf
                            </form>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">
                        No users
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
