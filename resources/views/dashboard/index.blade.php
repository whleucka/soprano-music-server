@extends('base.index')
@section('page-title', 'Dashboard')
@section('content-title', 'Dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div>You are logged in!</div>
            <div class="mt-3">
                <a class="btn btn-success btn-sm" href="http://soprano.hleuckanet.com/" title="Soprano"><i class="fas fa-headphones mr-2"></i> Soprano</a>
            </div>
        </div>
    </div>
@endsection
