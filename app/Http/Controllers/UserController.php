<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show all users in table format
     * @return Factory|View
     */
    public function index() {
        $users = User::all()
            ->sortBy('created_at');
        $me = Auth::user();
        return view('user.index', ['users' => $users, 'me' => $me]);
    }

    /**
     * Create new user form
     * @return Factory|View
     */
    public function create() {
        $user = new User();
        return view('user.create', compact('user'));
    }

    /**
     * Store new user in DB
     * @return RedirectResponse|Redirector
     */
    public function store() {
        $user = User::create($this->validated());
        return redirect("/users/{$user->id}");
    }

    /**
     * Universal validation function
     * @return array
     */
    protected function validated() {
        return request()->validate([
            'name' => 'required|min:3|max:50',
            'password' => 'required|confirmed|min:8',
            'email' => 'required|email',
        ]);
    }

    /**
     * Show user details
     * @param User $user
     * @return Factory|View
     */
    public function show(User $user) {
        return view('user.show', compact('user'));
    }

    /**
     * Edit user attributes
     * @param User $user
     * @return Factory|View
     */
    public function edit(User $user) {
        return view('user.edit', compact('user'));
    }

    /**
     * Update user in database
     * @param User $user
     * @return RedirectResponse|Redirector
     */
    public function update(User $user) {
        $user->update($this->validated());
        return redirect('/users');
    }

    /**
     * Delete user from database
     * @param User $user
     * @return RedirectResponse|Redirector
     */
    public function destroy(User $user) {
        try {
            $user->delete();
        } catch (Exception $e) {
            die("Fatal error: {$e->getMessage()}");
        }
        return redirect('/users');
    }
}
