@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fas fa-check mr-2"></i> <strong>Success</strong>
        <p class="pt-3">{{ $message }}</p>
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fas fa-bomb mr-2"></i> <strong>Error</strong>
        <p class="pt-3">{{ $message }}</p>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fas fa-exclamation-triangle mr-2"></i> <strong>Warning</strong>
        <p class="pt-3">{{ $message }}</p>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fas fa-exclamation-circle mr-2"></i> <strong>Info</strong>
        <p class="pt-3">{{ $message }}</p>
    </div>
@endif


{{--@if ($errors->any())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fas fa-exclamation mr-2"></i> <strong>Error</strong>
        <p class="pt-3">Please check the form below for errors</p>
    </div>
@endif--}}
