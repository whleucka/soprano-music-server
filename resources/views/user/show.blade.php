@extends('base.index')
@section('page-title', 'User')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="media">
                            <i class="fas fa-user-tie mr-2"></i>
                            <div class="media-body">
                                <h5 class="mt-1">{{ $user->name }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <caption class="caption-top">User Details:</caption>
                                <tbody class="border-bottom">
                                <tr>
                                    <td class="text-right pr-2" style="width: 100px;"><strong>ID</strong></td>
                                    <td>{{ $user->id }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Name</strong></td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>E-mail</strong></td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>API Token</strong></td>
                                    <td>{{ $user->api_token }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Role</strong></td>
                                    <td>{{ $user->role }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Created</strong></td>
                                    <td>{{ $user->created_at }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right pr-2"><strong>Updated</strong></td>
                                    <td>{{ $user->updated_at }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-primary mt-3" title="Back">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
