<?php

namespace App\Http\Controllers;

use App\MusicFile;
use Illuminate\Support\Facades\DB;

class MusicLibraryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $music_files = DB::table('music_files')
            ->orderBy('artist')
            ->orderBy('album');
        if (trim(request()->lib_search) !== '') {
            $term = trim(request()->lib_search) . '%';
            $music_files->where('artist', 'like', $term)
                ->orWhere('album', 'like', $term)
                ->orWhere('title', 'like', '%' . $term);
        }
        return view('music_library.index', [
            'music_files' => $music_files->paginate(50)
        ]);
    }
}
