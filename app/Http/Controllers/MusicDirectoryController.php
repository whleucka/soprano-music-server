<?php

namespace App\Http\Controllers;

use App\MusicDirectory;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class MusicDirectoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return redirect('/music-settings');
    }

    /**
     * Store new directory in DB
     * @return RedirectResponse|Redirector
     */
    public function store() {
        if ($dir = MusicDirectory::create($this->validated()))
            request()->session()->flash('success', "Successfully added new directory path");
        else
            request()->session()->flash('danger', "Couldn't add new directory path");
        return redirect("/music-settings");
    }

    /**
     * Universal validation function
     * @return array
     */
    protected function validated() {
        return request()->validate([
            'path' => [
                'required',
                'string',
                Rule::unique('music_directories'),
                function ($attribute, $value, $fail) {
                    if (!file_exists($value))
                        $fail("Path not found. Please check the path and try again.");
                }
            ]
        ]);
    }

    /**
     * Delete directory from database
     * @param MusicDirectory $directory
     * @return RedirectResponse|Redirector
     */
    public function destroy(MusicDirectory $directory) {
        try {
            if ($directory->delete()) {
                request()->session()->flash('success', "Successfully deleted directory path");
            } else {
                request()->session()->flash('danger', "Couldn't delete directory path");
            }
        } catch (Exception $e) {
            die("Fatal error: {$e->getMessage()}");
        }
        return redirect('/music-settings');
    }

    /**
     * Create new directory form
     * @return Factory|View
     */
    public function create() {
        $dir = new MusicDirectory();
        return view('music_directory.create', compact('dir'));
    }
}
