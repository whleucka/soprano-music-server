@extends('base.index')
@section('page-title', 'Music Library')
@section('content-title', 'Music Library')

@section('content')
    <div>
        <form method="GET">
            @csrf
            <div class="input-group">
                <input value="{{ Request::get('lib_search') }}" name="lib_search" id="lib-search" type="text" class="form-control" placeholder="Artist, Album, or Track" aria-label="library search" aria-describedby="library-search">
                <div class="input-group-append">
                    <button class="btn btn-success" id="search-submit" type="submit">Search</button>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-secondary" id="search-reset" type="reset" onClick="document.getElementById('lib-search').value = ''; document.getElementById('search-submit').click();">Clear</button>
                </div>
            </div>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-sm table-hover">
            <caption class="caption-top">
                Found {{ number_format($music_files->total()) }} results
            </caption>
            <thead>
            <tr>
                <th>ID</th>
                <th style="width: 28px;"></th>
                <th>Album</th>
                <th>Artist</th>
                <th>Title</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($music_files as $file)
                <tr>
                    <td><a href="/music-files/{{ $file->id }}"><div class="truncate" title="{{ $file->id }}">{{ $file->id }}</div></a></td>
                    <th><img class="album-cover-art" src="{{ $file->cover }}" alt="cover" title="{{ $file->album }}"></th>
                    <td><div class="truncate" title="{{ $file->album }}">{{ $file->album }}</div></td>
                    <td><div class="truncate" title="{{ $file->artist }}">{{ $file->artist }}</div></td>
                    <td><div class="truncate" title="{{ $file->title }}">{{ $file->title }}</div></td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">
                        No music files
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="mt-4">
        {{ $music_files->onEachSide(1)->appends($_GET)->links() }}
    </div>
@endsection
