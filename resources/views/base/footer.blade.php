<footer class="bg-dark text-white-50 pt-0">
    <div class="container">
        <p class="p-0 m-0 text-center">
            <small>&copy; <?=date('Y')?> <i class="fas fa-music text-success mx-1"></i> {{ config('app.name', 'Laravel') }} created by <a class="text-white"
                    href="mailto:william.hleucka@gmail.com?subject=Mail @ <?=date('Y-m-d H:i:s');?>">William Hleucka</a> <i class="fab fa-canadian-maple-leaf text-danger mx-1" title="Oh, Canada!"></i></small>
        </p>
    </div>
</footer>
