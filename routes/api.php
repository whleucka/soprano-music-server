<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Example user route
Route::middleware('auth:api')->get('/user', function (Request $request) {return $request->user();});

/*
 * Soprano Server API endpoints
 */
Route::middleware('auth:api')->get('/auth', 'MusicLibraryAPIController@auth');
Route::middleware('auth:api')->get('/all-tracks', 'MusicLibraryAPIController@tracks');
Route::middleware('auth:api')->get('/all-artists', 'MusicLibraryAPIController@artists');
Route::middleware('auth:api')->get('/artist', 'MusicLibraryAPIController@artist');
Route::middleware('auth:api')->get('/all-albums', 'MusicLibraryAPIController@albums');
Route::middleware('auth:api')->get('/album', 'MusicLibraryAPIController@album');
Route::middleware('auth:api')->get('/like', 'MusicLibraryAPIController@like');
Route::middleware('auth:api')->get('/liked', 'MusicLibraryAPIController@getLiked');
