<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome.index');
});

Route::get('/music-directories/create', 'MusicDirectoryController@create');
Route::delete('/music-directories/{directory}', 'MusicDirectoryController@destroy');
Route::post('/music-directories', 'MusicDirectoryController@store');
Route::get('/music-directories', 'MusicDirectoryController@index');

Route::get('/music-settings', 'MusicSettingsController@index');
Route::post('/music-settings/scan/{directory}', 'MusicSettingsController@scan');
Route::post('/music-settings/scan-all', 'MusicSettingsController@scan_all');

Route::get('/music-library', 'MusicLibraryController@index');

Route::get('/music-files/{file}', 'MusicFileController@show');

Route::get('/stream/{file}', 'MusicStreamController@stream');

Route::get('/users', 'UserController@index');
Route::get('/users/{user}', 'UserController@show');
Route::delete('/users/{user}', 'UserController@destroy');

#Auth::routes();
Auth::routes(['verify' => true, 'register' => false]);

Route::get('/dashboard', 'DashboardController@index')->name('home');
