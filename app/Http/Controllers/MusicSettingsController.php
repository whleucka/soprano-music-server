<?php

namespace App\Http\Controllers;

use getID3;
use App\MusicDirectory;
use App\MusicFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use SplFileInfo;
use Exception;

class MusicSettingsController extends Controller
{
    private $id3 = null;
    private $file_update = 0;
    /**
     * Audio formats
     * @var array
     */
    private $accepted_formats = ['mp3', 'm4a', 'flac', 'ogg', 'wav'];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Music settings view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $directories = MusicDirectory::all();

        return view('music_settings.index', [
            'directories' => $directories,
            'accepted_formats' => join(', ', $this->accepted_formats)
        ]);
    }

    /**
     * Scan directory from post
     * @param MusicDirectory $directory
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \getid3_exception
     */
    public function scan(MusicDirectory $directory) {
        $this->scanDirectory($directory);
        $this->removeOrphans();
        return redirect('music-settings');
    }

    /**
     * Scan all directories from post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \getid3_exception
     */
    public function scan_all() {
        $directories = MusicDirectory::all();
        foreach ($directories as $dir) {
            $this->scanDirectory($dir);
        }
        $this->removeOrphans();
        return redirect('music-settings');
    }

    /**
     * Recursively scan for files in directories
     * @param MusicDirectory $directory
     * @throws \getid3_exception
     */
    public function scanDirectory(MusicDirectory $directory) {
        $di = new RecursiveDirectoryIterator($directory->getAttribute('path'), RecursiveDirectoryIterator::SKIP_DOTS);
        $it = new RecursiveIteratorIterator($di);
        DB::beginTransaction();
        foreach ($it as $file) {
            if (in_array(pathinfo($file, PATHINFO_EXTENSION), $this->accepted_formats)) {
                if ($this->synchFile($file))
                    $this->file_update++;
            }
        }
        DB::commit();
        if ($this->file_update > 0)
            request()->session()->flash('success', "Synchronized {$this->file_update} files to the database");
        else
            request()->session()->flash('warning', "Synchronized 0 files to the database");
    }

    public function removeOrphans() {
        $all = MusicFile::all();
        foreach ($all as $one) {
            if (!file_exists($one->file_path))
                try {
                    $one->delete();
                } catch (Exception $e) {
                    error_log("Couldn't delete music file asset: {$e->getMessage()}");
                }
        }
    }

    /**
     * Analyze file and return ID3 tag info
     * @param string $file_path
     * @return array
     * @throws \getid3_exception
     */
    public function getTagInfo(string $file_path) {
        if (!$this->id3)
            $this->id3 = new getID3;
        return $this->id3->analyze($file_path);
    }

    /**
     * Get cover art path for file when directory is scanned
     * @param SplFileInfo $file
     * @return string
     */
    public function getCover(SplFileInfo $file) {
        $path = $file->getPath();
        $covers = [
            $path . '/cover.jpg',
            $path . '/cover.jpeg',
            $path . '/cover.png'
        ];
        foreach ($covers as $cover) {
            if (file_exists($cover)) {
                // This cover exists in the same directory as the audio file
                // Get some file information
                $cover_info = pathinfo($cover);
                $cover_ext = $cover_info['extension'];
                $cover_name = md5($cover);
                // Make the storage dir if it doesn't exist
                $storage_dir = storage_path() . '/app/public/covers/';
                if (!file_exists($storage_dir))
                    mkdir($storage_dir);
                $storage_path = $storage_dir . $cover_name . '.' . $cover_ext;
                $public_path = '/storage/covers/'. $cover_name . '.' . $cover_ext;
                if (!file_exists($storage_path)) {
                    File::copy($cover, $storage_path);
                    return $public_path;
                } else {
                    return $public_path;
                }
            }
        }
        return '/img/no-album-art.png';
    }

    /**
     * Synchronize file to database
     * @param SplFileInfo $file
     * @return bool
     * @throws \getid3_exception
     */
    public function synchFile(SplFileInfo $file): bool {
        $file_name = $file->getFilename();
        $file_path = $file->getPathname();
        $cover = $this->getCover($file);
        $tag = $this->getTagInfo($file_path);
        $music_file = [
            'cover' => $cover,
            'file_path' => $file_path,
            'file_name' => $file_name,
            'file_size' => intval($tag['filesize'] ?? 0),
            'file_format' => $tag['fileformat'],
            'bitrate' => intval($tag['bitrate'] ?? 0),
            'playtime_string' => $tag['playtime_string'] ?? null,
            'playtime_seconds' => $tag['playtime_seconds'] ?? null,
            'mime_type' => $tag['mime_type'] ?? null,
        ];
        switch ($tag['fileformat']) {
            case 'mp3':
                if (isset($tag['tags']['id3v2'])) {
                    $id3type = 'id3v2';
                } else if (isset($tag['tags']['id3v1'])) {
                    $id3type = 'id3v1';
                }
                if (isset($id3type)) {
                    $year = $tag['tags'][$id3type]['year'][0] ?? null;
                    if (strlen($year) > 4)
                        $year = date('Y', strtotime($year)) ?? null;
                    $track_no = intval($tag['tags'][$id3type]['track_number'][0] ?? 0);
                    $music_file['title'] = $tag['tags'][$id3type]['title'][0] ?? 'No Title';
                    $music_file['artist'] = $tag['tags'][$id3type]['artist'][0] ?? 'No Artist';
                    $music_file['album'] = $tag['tags'][$id3type]['album'][0] ?? 'No Album';
                    $music_file['year'] =  $year ?? null;
                    $music_file['track_number'] = $track_no;
                }
                break;
            case 'flac':
            case 'ogg':
                if (isset($tag['tags']['vorbiscomment'])) {
                    $year = $tag['tags']['vorbiscomment']['date'][0] ?? null;
                    if (strlen($year) > 4)
                        $year = date('Y', strtotime($year)) ?? null;
                    $track_no = intval($tag['tags']['vorbiscomment']['tracknumber'][0] ?? 0);
                    $music_file['title'] = $tag['tags']['vorbiscomment']['title'][0] ?? 'No Title';
                    $music_file['artist'] = $tag['tags']['vorbiscomment']['artist'][0] ?? 'No Artist';
                    $music_file['album'] = $tag['tags']['vorbiscomment']['album'][0] ?? 'No Album';
                    $music_file['year'] = $year ?? null;
                    $music_file['track_number'] = $track_no;
                }
                break;
        }
        // Update existing record or create a new record
        // Note that the first arg is the unique key / value
        // The second arg is the insert/update arr
        if (MusicFile::updateOrCreate(['md5_file_path' => md5($file_path)], $music_file))
            return true;
        return false;
    }
}
