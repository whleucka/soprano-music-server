<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} | @yield('page-title', 'Music Server')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    @yield('head-css')
    @yield('head-scripts')
</head>
<body>
@component('base.navigation')
    Top Navigation
@endcomponent
<div id="content" class="container p-3 p-md-5">
    @if (!empty($__env->yieldContent('content-title')))
        <h2 class="content-title mb-3 mb-md-5">@yield('content-title')</h2>
    @endif
    @include('base.flash-message')
    @yield('content')
</div>
@component('base.footer')
    Bottom Footer
@endcomponent
@yield('body-scripts')
</body>
</html>
