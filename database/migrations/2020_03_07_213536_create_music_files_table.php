<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_files', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('md5_file_path')->unique()->index();
            $table->text('file_path');
            $table->string('file_name');
            $table->integer('file_size')->unsigned();
            $table->char('file_format', 6);
            $table->integer('bitrate')->unsigned();
            $table->string('album')->default("No Album");
            $table->string('artist')->default("No Artist");
            $table->string('title')->default("No Title");
            $table->string('cover')->nullable(true)->index();
            $table->text('track_number')->nullable(true);
            $table->string('playtime_string', 15)->nullable(true);
            $table->decimal('playtime_seconds', 8, 2)->nullable(true);
            $table->string('mime_type')->nullable(true);
            $table->char('year', 4)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_files');
    }
}
