<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class MusicFile extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * Important for setting id as UUID
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::uuid4();
            $model->md5_file_path = md5($model->file_path);
        });
    }

    protected $guarded = [];
}
