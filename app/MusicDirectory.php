<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicDirectory extends Model
{
    /**
     * Set MD5 path
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->md5_path = (string) md5($model->path);
        });
    }

    protected $guarded = []; // YOLO
}
