window.formConfirm = function (e, form, msg, animate = false) {
    e.preventDefault();
    if (confirm(msg)) {
        if (animate)
            e.currentTarget.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>\n' +
                '  <span class="sr-only">Loading...</span>';
        return form.submit();
    }
    return false;
};
